# Introduction

This project creates a docker image with (open)jdk 8 and maven 3 installed.

The image can be used to run maven builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk8-maven3
